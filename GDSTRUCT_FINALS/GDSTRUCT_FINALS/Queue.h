#pragma once
#include <string>
#include <iostream>
using namespace std;

struct Queue {
	int front, back, storage;
	int* queue;
	Queue(int store) {
		front = back = 0;
		storage = store;
		queue = new int;
	}

	~Queue() { delete[] queue; }
	//Insert "Push" an element
virtual void qPush(int val) {
		//isFull
		if (storage == back) {
			printf("Queue is Full. Please remove elements");
			cout << endl;
			return;
		}
		//insert at the  end
		else {
			queue[back] = val;
			back++;
		}
		return;
	}
	//Delete "Pop" an element
	void qPop(){
			for (int i = 0; i < back - 1; i++) {
				queue[i] = queue[i + 1];
			}
			back--;
	}
	//print elements in queue
	void qDisplay() {	
		for (int i = front; i < back; i++) {
			printf("%d \n", queue[i]);
		}
		return;
	}
	//Print the "Top" /Front of the queue
	void qPushFront() {
		if (front == back) {
			printf("Queue is empty. add an element");
			return;
		}
		printf("Queue: %d", queue[front]);
		return;
	}
	//Delete all elements
	void qRemove() {
		printf("Queue has been deleted");
		cout << endl;
			for (int i = 0; i < back - 1; i++) {
				qPop();
			}
			back--;
		
	}
};