#pragma once
#include <string>
#include <iostream>
using namespace std;

class Stack{
private:
	int top, storage;
	int* stack;

public:
	Stack(int size) {
		top = -1;
		storage = size - 1;
		stack = new int[size];
	}

	int isEmpty() {
		if (top == -1) return 1; else return 0;
	};
	int isFull() {
		if (top == storage)  return 1; else return 0;
	};
	~Stack() {	delete[] stack;	}

	//Insert "Push" an element
	virtual void sPush(int val) {
		if (isFull() == 1) {
			printf("Stack is Full. Please remove elements");
			cout << endl;
			return;
		}
		else
		stack[++top] = val;

	}
	//Delete "Pop" elements
	int sPop() {
		stack[top--];
		return 0;
	}
	//print elements in queue
	void sDisplay() {
	
		int i;
		for (i = top; i >= 0; i--){
			printf("%d \n", stack[i]);
		}
		return;
	}
	//Print the "Top"/Front element
	void sPushFront() {
		printf("Stack: %d", stack[top]);
	}
	//Delete all elements
	void sRemove() {
		printf("Stack has been deleted.");
		cout << endl;
			for (int i = top; i >= 0; i--) {
				sPop();
			}
	}

};