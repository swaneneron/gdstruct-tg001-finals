
#include <string>
#include "Stack.h"
#include "Queue.h"
#include <iostream>
int main(void){

	int val;
	cout << "Enter the size of element sets: ";
	int size;
	cin >> size;

	Queue q(size);
	Stack s(size);

	while(true) {
		int choice;
		cout << "What would you like to do?" << endl;
		cout << " 1. Push elements \n 2. Pop elements \n 3. Print elements \n 4. Remove all set elements" << endl;

		cin >> choice;
		switch (choice) {
		case 1:
			cout << "Enter the number to be enqueued: ";
			cin >> val;
			q.qPush(val);
			s.sPush(val);
			cout << endl;
			q.qPushFront();
			cout << endl;
			s.sPushFront();
			cout << endl;	
			break;
		case 2:
			cout << "You have popped the front elements" << endl;
			q.qPop();
			s.sPop();
			q.qPushFront();
			cout << endl;
			s.sPushFront();
			cout << endl;
			break;
		case 3:
			cout << "Queue:" << endl;
			q.qDisplay();
			cout << "Stack:" << endl;
			s.sDisplay();
			cout << endl;
			break;
		case 4:
			cout << "The set has been successfully cleared!" << endl;
			q.qRemove();
			s.sRemove();
		}
		
		system("pause");
		system("cls");
}
}